# OpenBSD Ports

These are ports for the OpenBSD ports tree, all in varied states of completion. Some are fully functional, others work in progress or abandoned for the time being.

Some or most are heavily influenced by their FreeBSD counterparts (lxappearance &co. for example).

# License

These ports themselves are freely available under the [BSD license](https://opensource.org/licenses/BSD-3-Clause).
